Icon paint.png, save.png, folder.png made by Maxim Basinksi under CC 3.0 BY License from icons http://www.flaticon.com/authors/maxim-basinski

Icon close.png, left-arrow.png made by Madebyoliver under Flaticon Basic License from http://www.flaticon.com/authors/madebyoliver

pencil.png, open-book.png Icons made by "http://www.flaticon.com/authors/vectors-market" "Vectors Market" Vectors Market from "http://www.flaticon.com" "Flaticon">www.flaticon.com is licensed by "http://creativecommons.org/licenses/by/3.0/" "Creative Commons BY 3.0" "_blank" CC 3.0 BY

one.png, two.png, three.png, four.png, five.png, six.png, seven.png, eight.png Icons made by http://www.flaticon.com/authors/roundicons in interface on http://www.flaticon.com and is licensed by Flaticon Basic License

Turned all .png into .ico to be used by windows form through http://convertico.com/