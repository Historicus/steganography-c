﻿/*
Author: Nick Schafhauser, Samuel Kenney, Richie Covatto
Course: Comp 322 A
Date: 12/13/2016
Description: C# implementation of Steganography program. Allows bit encoding of 1 - 8 bits.
Intended Purpose: Allow a user to hide and retrieve secret messages from a bit map.
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Steganography_CSharp
{
    public partial class Stegonagraphy : Form
    {
        //create variables for the rest of the program
        private string filename;
        private int totalAmount;
        private int amountLeft;
        private int chosen;

        public Stegonagraphy()
        {
            InitializeComponent();
            ToolTip ToolTipOpen = new System.Windows.Forms.ToolTip();
            ToolTipOpen.SetToolTip(this.OpenBtn, "Open");

            ToolTip ToolTipSave = new System.Windows.Forms.ToolTip();
            ToolTipSave.SetToolTip(this.SaveBtn, "Save");

            ToolTip ToolTipClose = new System.Windows.Forms.ToolTip();
            ToolTipClose.SetToolTip(this.CloseBtn, "Close");

            ToolTip ToolTipExit = new System.Windows.Forms.ToolTip();
            ToolTipExit.SetToolTip(this.ExitBtn, "Exit");

            ToolTip ToolTipRead = new System.Windows.Forms.ToolTip();
            ToolTipRead.SetToolTip(this.ReadBtn, "Read");

            ToolTip ToolTipWrite = new System.Windows.Forms.ToolTip();
            ToolTipWrite.SetToolTip(this.WriteBtn, "Write");
        }

        public SteganographyCodex bitmapManager;

        //allows for the opening of a bitmap and putting it in the picture box
        private void OpenBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openBitmap = new OpenFileDialog();

            openBitmap.Filter = "Bitmap Files (*.bmp)|*.bmp|Bitmap Files (*.dib)|*.dib";
            openBitmap.RestoreDirectory = true;

            try
            {
                //opens up a dialog for choosing a file
                if (openBitmap.ShowDialog() == DialogResult.OK)
                {
                    //grabs the file from the one chosen in the dialog
                    filename = openBitmap.FileName;
                    BitmapPictureBox.Image = Bitmap.FromFile(filename);
                    BitmapPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;

                    //enables most of the features of the GUI for the user
                    ReadBtn.Enabled = true;
                    WriteBtn.Enabled = true;
                    MessageTextBox.Enabled = true;
                    SaveBtn.Enabled = true;
                    CloseBtn.Enabled = true;
                    OneBtn.Enabled = true;
                    TwoBtn.Enabled = true;
                    ThreeBtn.Enabled = true;
                    FourBtn.Enabled = true;
                    FiveBtn.Enabled = true;
                    SixBtn.Enabled = true;
                    SevenBtn.Enabled = true;
                    EightButn.Enabled = true;
                    OneBtn.BackColor = SystemColors.ControlDark;
                    TwoBtn.BackColor = SystemColors.Control;
                    ThreeBtn.BackColor = SystemColors.Control;
                    FourBtn.BackColor = SystemColors.Control;
                    FiveBtn.BackColor = SystemColors.Control;
                    SixBtn.BackColor = SystemColors.Control;
                    SevenBtn.BackColor = SystemColors.Control;
                    EightButn.BackColor = SystemColors.Control;

                    //default encoding is for one bit
                    chosen = 1;

                    MessageTextBox.ResetText();

                    //figures out how much is left to write into the picture
                    totalAmount = ((((BitmapPictureBox.Image.Height * BitmapPictureBox.Image.Width)*3)/8)-4);
                    AmountLeftLbl.Text = "0/" + totalAmount;

                    MessageTextBox.MaxLength = totalAmount;

                    //instatiates the class for our functionality
                    bitmapManager = new SteganographyCodex(filename);
                }
            }
            catch (Exception ex)
            {
                const string message =
                    "Please select another file to open.";
                const string caption = "Could not open. ";
                MessageBox.Show(message, caption,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        //allows for the saving of newly written to files
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveBitmap = new SaveFileDialog();

            saveBitmap.Filter = "Bitmap Files (*.bmp)|*.bmp|Bitmap Files (*.dib)|*.dib";
            saveBitmap.RestoreDirectory = true;
            try
            {
                //opens dialog to choose what to save to
                if (saveBitmap.ShowDialog() == DialogResult.OK)
                {
                    //saves file with filename given, will overwrite previous file if same name
                    filename = saveBitmap.FileName;
                    bitmapManager.writeImage(filename);
                }
            }
            catch (Exception ex)
            {
                const string message =
                    "Please select another file to save.";
                const string caption = "Could not save file.";
                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);

            } 
            
        }

        //closes down the GUI to what it was when it was started
        private void CloseBtn_Click(object sender, EventArgs e)
        {
            BitmapPictureBox.Image = BitmapPictureBox.InitialImage;
            BitmapPictureBox.SizeMode = PictureBoxSizeMode.CenterImage;

            ReadBtn.Enabled = false;
            WriteBtn.Enabled = false;
            MessageTextBox.Enabled = false;
            SaveBtn.Enabled = false;
            CloseBtn.Enabled = false;
            OneBtn.Enabled = false;
            TwoBtn.Enabled = false;
            ThreeBtn.Enabled = false;
            FourBtn.Enabled = false;
            FiveBtn.Enabled = false;
            SixBtn.Enabled = false;
            SevenBtn.Enabled = false;
            EightButn.Enabled = false;
            OneBtn.BackColor = SystemColors.Control;
            TwoBtn.BackColor = SystemColors.Control;
            ThreeBtn.BackColor = SystemColors.Control;
            FourBtn.BackColor = SystemColors.Control;
            FiveBtn.BackColor = SystemColors.Control;
            SixBtn.BackColor = SystemColors.Control;
            SevenBtn.BackColor = SystemColors.Control;
            EightButn.BackColor = SystemColors.Control;
            MessageTextBox.ResetText();
        }

        //exits the application
        private void ExitBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BitmapPictureBox_Click(object sender, EventArgs e)
        {
        }

        //reads the message and encodes
        private void ReadBtn_Click(object sender, EventArgs e)
        {
           string message = bitmapManager.getMessage(chosen);
           MessageTextBox.Text = message;
        }

        //decodes the message
        private void WriteBtn_Click(object sender, EventArgs e)
        {
            string message = MessageTextBox.Text;
            bitmapManager.setMessage(message, chosen);

        }

        //changes label to know how many characters you have left to write
        private void MessageTextBox_TextChanged(object sender, EventArgs e)
        {
            amountLeft = MessageTextBox.Text.Length;

            AmountLeftLbl.Text = amountLeft + "/" + totalAmount;
        }

        private void AmountLeftLbl_Click(object sender, EventArgs e)
        {

        }

        //highlights button when clicked and changes value of amoutn of characters that can be written
        private void OneBtn_Click_1(object sender, EventArgs e)
        {
            chosen = 1;
            OneBtn.BackColor = SystemColors.ControlDark;
            TwoBtn.BackColor = SystemColors.Control;
            ThreeBtn.BackColor = SystemColors.Control;
            FourBtn.BackColor = SystemColors.Control;
            FiveBtn.BackColor = SystemColors.Control;
            SixBtn.BackColor = SystemColors.Control;
            SevenBtn.BackColor = SystemColors.Control;
            EightButn.BackColor = SystemColors.Control;
            totalAmount = ((((BitmapPictureBox.Image.Height * BitmapPictureBox.Image.Width) * 3) / 8) - 4);
            AmountLeftLbl.Text = amountLeft + "/" + totalAmount;
            MessageTextBox.MaxLength = totalAmount;
            MessageTextBox.ResetText();

        }

        //highlights button when clicked and changes value of amoutn of characters that can be written
        private void TwoBtn_Click(object sender, EventArgs e)
        {
            chosen = 2;
            OneBtn.BackColor = SystemColors.Control;
            TwoBtn.BackColor = SystemColors.ControlDark;
            ThreeBtn.BackColor = SystemColors.Control;
            FourBtn.BackColor = SystemColors.Control;
            FiveBtn.BackColor = SystemColors.Control;
            SixBtn.BackColor = SystemColors.Control;
            SevenBtn.BackColor = SystemColors.Control;
            EightButn.BackColor = SystemColors.Control;
            totalAmount = (((((BitmapPictureBox.Image.Height * BitmapPictureBox.Image.Width) * 3) * 2) / 8) - 4);
            AmountLeftLbl.Text = amountLeft + "/" + totalAmount;
            MessageTextBox.MaxLength = totalAmount;
            MessageTextBox.ResetText();
        }

        //highlights button when clicked and changes value of amoutn of characters that can be written
        private void ThreeBtn_Click(object sender, EventArgs e)
        {
            chosen = 3;
            OneBtn.BackColor = SystemColors.Control;
            TwoBtn.BackColor = SystemColors.Control;
            ThreeBtn.BackColor = SystemColors.ControlDark;
            FourBtn.BackColor = SystemColors.Control;
            FiveBtn.BackColor = SystemColors.Control;
            SixBtn.BackColor = SystemColors.Control;
            SevenBtn.BackColor = SystemColors.Control;
            EightButn.BackColor = SystemColors.Control;
            totalAmount = (((((BitmapPictureBox.Image.Height * BitmapPictureBox.Image.Width) * 3) * 3) / 8) - 4);
            AmountLeftLbl.Text = amountLeft + "/" + totalAmount;
            MessageTextBox.MaxLength = totalAmount;
            MessageTextBox.ResetText();
        }

        //highlights button when clicked and changes value of amoutn of characters that can be written
        private void FourBtn_Click(object sender, EventArgs e)
        {
            chosen = 4;
            OneBtn.BackColor = SystemColors.Control;
            TwoBtn.BackColor = SystemColors.Control;
            ThreeBtn.BackColor = SystemColors.Control;
            FourBtn.BackColor = SystemColors.ControlDark;
            FiveBtn.BackColor = SystemColors.Control;
            SixBtn.BackColor = SystemColors.Control;
            SevenBtn.BackColor = SystemColors.Control;
            EightButn.BackColor = SystemColors.Control;
            totalAmount = (((((BitmapPictureBox.Image.Height * BitmapPictureBox.Image.Width) * 3)* 4) / 8) - 4);
            AmountLeftLbl.Text = amountLeft + "/" + totalAmount;
            MessageTextBox.MaxLength = totalAmount;
            MessageTextBox.ResetText();
        }

        //highlights button when clicked and changes value of amoutn of characters that can be written
        private void FiveBtn_Click(object sender, EventArgs e)
        {
            chosen = 5;
            OneBtn.BackColor = SystemColors.Control;
            TwoBtn.BackColor = SystemColors.Control;
            ThreeBtn.BackColor = SystemColors.Control;
            FourBtn.BackColor = SystemColors.Control;
            FiveBtn.BackColor = SystemColors.ControlDark;
            SixBtn.BackColor = SystemColors.Control;
            SevenBtn.BackColor = SystemColors.Control;
            EightButn.BackColor = SystemColors.Control;
            totalAmount = (((((BitmapPictureBox.Image.Height * BitmapPictureBox.Image.Width) * 3)* 5) / 8) - 4);
            AmountLeftLbl.Text = amountLeft + "/" + totalAmount;
            MessageTextBox.MaxLength = totalAmount;
            MessageTextBox.ResetText();
        }

        //highlights button when clicked and changes value of amoutn of characters that can be written
        private void SixBtn_Click(object sender, EventArgs e)
        {
            chosen = 6;
            OneBtn.BackColor = SystemColors.Control;
            TwoBtn.BackColor = SystemColors.Control;
            ThreeBtn.BackColor = SystemColors.Control;
            FourBtn.BackColor = SystemColors.Control;
            FiveBtn.BackColor = SystemColors.Control;
            SixBtn.BackColor = SystemColors.ControlDark;
            SevenBtn.BackColor = SystemColors.Control;
            EightButn.BackColor = SystemColors.Control;
            totalAmount = (((((BitmapPictureBox.Image.Height * BitmapPictureBox.Image.Width) * 3)* 6) / 8) - 4);
            AmountLeftLbl.Text = amountLeft + "/" + totalAmount;
            MessageTextBox.MaxLength = totalAmount;
            MessageTextBox.ResetText();
        }

        //highlights button when clicked and changes value of amoutn of characters that can be written
        private void SevenBtn_Click(object sender, EventArgs e)
        {
            chosen = 7;
            OneBtn.BackColor = SystemColors.Control;
            TwoBtn.BackColor = SystemColors.Control;
            ThreeBtn.BackColor = SystemColors.Control;
            FourBtn.BackColor = SystemColors.Control;
            FiveBtn.BackColor = SystemColors.Control;
            SixBtn.BackColor = SystemColors.Control;
            SevenBtn.BackColor = SystemColors.ControlDark;
            EightButn.BackColor = SystemColors.Control;
            totalAmount = (((((BitmapPictureBox.Image.Height * BitmapPictureBox.Image.Width) * 3) * 7) / 8) - 4);
            AmountLeftLbl.Text = amountLeft + "/" + totalAmount;
            MessageTextBox.MaxLength = totalAmount;
            MessageTextBox.ResetText();
        }

        //highlights button when clicked and changes value of amoutn of characters that can be written
        private void EightButn_Click(object sender, EventArgs e)
        {
            chosen = 8;
            OneBtn.BackColor = SystemColors.Control;
            TwoBtn.BackColor = SystemColors.Control;
            ThreeBtn.BackColor = SystemColors.Control;
            FourBtn.BackColor = SystemColors.Control;
            FiveBtn.BackColor = SystemColors.Control;
            SixBtn.BackColor = SystemColors.Control;
            SevenBtn.BackColor = SystemColors.Control;
            EightButn.BackColor = SystemColors.ControlDark;
            totalAmount = (((((BitmapPictureBox.Image.Height * BitmapPictureBox.Image.Width) * 3) * 8) / 8) - 4);
            AmountLeftLbl.Text = amountLeft + "/" + totalAmount;
            MessageTextBox.MaxLength = totalAmount;
            MessageTextBox.ResetText();
        }
    }

    public class SteganographyCodex
    {
        private byte[] rawBytes; //private byte array that holds the picture byte by byte
        private Bitmap image1; // the image of the bitmap

        private int width //the width of the bitmap
        {
            get { return width; }
            set { width = value; }
        }
        private int height //the height of the bitmap
        {
            get { return height; }
            set { height = value; }
        }

        public SteganographyCodex(string filename) //filename of the file
        {
            rawBytes = null;
            rawBytes = loadBMP(filename);
        }

        //retrieves the secret message from the bitmap
        public string getMessage(int chosen)
        {
           
            byte[] magicNumber = Encoding.ASCII.GetBytes("316"); //byte is supposed to be the closest thing to unsigned char
            
            byte mask = 0x1; //0000 00001. Allows us to pick and choose which bit we want via left shifts.
            uint currCharIndex = 0; //current index in the rawBytes array
            byte constructChar; //a byte that we will be putting together via the extracted bits from rawBytes array.
            
            int numBitsThatHaveBeenEncoded = 0; //the amount of bits that have been inserted into the constructChar byte.
            int numBitsToBeEncoded = chosen; //depends on which bit encoding button the user selects. Can be in range of {1, 8}
            int bitsOfConstructCharEncoded = 0; //keeps track of how many bits have been inserted into the constructorChar byte
            int placeHolder = 0; //0 or 1 depending on which bit has been masked out of the current rawBytes[index]. This is how build our secret message.
            int rawBytesCounter = 0; //used only when 5 or 7 bits are selected. Helps keep track on if there are any secret bits mixed in with a byte that also contained magic number bits.

            byte[] buffer = new byte[3]; //holds 3 because there are 3 magic numbers. 316
            for (int i = 0; i < 3;)
            {
                constructChar = 0;
                //j < 8 because there are 8 bits in a byte.
                for (int j = 0; j < 8 && i < 3; j++)
                {
                    //p < numBitsToBeEncoded because if the user chose 2 bit encoding then we want to grab 2 bits from each byte.
                    for (int p = 0; p < numBitsToBeEncoded && i < 3; p++)
                    {
                        //masks rawBytes[currCharIndex] with 0001. The mask is left shifted depending on which bit encoding was selected.
                        if (((rawBytes[currCharIndex] & (mask << p)) != 0))
                        {
                            placeHolder = 1; //becomes 1 if the bit that was masked out from rawBytes was not 0
                        }
                        else
                            placeHolder = 0;

                        //write the bit back into the constructChar. As the for-loop progresses, the placeHolder will be left shifted.
                            //this is so that the constructChar can be built up 1 bit at a time.
                        constructChar |= (byte)(placeHolder << bitsOfConstructCharEncoded);

                        //increment counters
                        bitsOfConstructCharEncoded++;
                        numBitsThatHaveBeenEncoded++;
                        rawBytesCounter++;

                        //if we have retrieved numBitsToBeEncoded bits from a byte, then we want to grab the next byte from our rawBytes[]
                        if (rawBytesCounter == numBitsToBeEncoded)
                        {
                            currCharIndex++;
                            rawBytesCounter = 0; //reset the counter because we are now starting at the first bit in the next byte.
                        }

                        if (bitsOfConstructCharEncoded == 8)
                        {
                            //once we have completed building our constructChar byte, we want to reset our location tracker to the first bit of the next one.
                            bitsOfConstructCharEncoded = 0; 
                        }

                        //this conditional is triggered once 8 bits have been extracted from the secret bits.
                        if (numBitsThatHaveBeenEncoded == 8)
                        {
                            numBitsThatHaveBeenEncoded = 0;
                            buffer[i] = constructChar; //places the magic number into a buffer array.
                            constructChar = 0;
                            i++; //increments i so that we begin extracting bits from the next magic number.
                        }
                    }
                }
            }

            //if the magic numbers do not match up with their respective buffer, then this bit map has not been altered.
            if (buffer[0] != magicNumber[0] || buffer[1] != magicNumber[1] || buffer[2] != magicNumber[2])
            {
                return ""; //return an empty string that will be displayed in the messageTextBox.
            }

            string makeMessage = null; //used a string instead of an ostringstream it might cause bugs
            bitsOfConstructCharEncoded = 0;
            numBitsThatHaveBeenEncoded = 0;

            bool loopDone = false; //this bool turns true once a null character has been read in.

            constructChar = 0;

            //this do-while continually creates tempConstructChar from the extracted secret bits until a null character is hit
            do
            {
                byte[] tempConstructChar = new byte[1];
                for (int i = 0; i < 8/numBitsToBeEncoded; i++)
                {
                    //this for-loop runs for however many bits there are that the user has selected to encode.
                    for (int p = rawBytesCounter; p < numBitsToBeEncoded && !loopDone; p++)
                    {
                        if (p == -1)
                            p = 1;

                        //masks rawBytes[currCharIndex] with 0001. The mask is left shifted depending on which bit encoding was selected.
                        if (((rawBytes[currCharIndex] & (mask << p)) != 0))
                        {
                            placeHolder = 1; //becomes 1 if the bit that was masked out from rawBytes was not 0
                        }
                        else
                            placeHolder = 0;

                        //write the bit back into the constructChar. As the for-loop progresses, the placeHolder will be left shifted.
                            //this is so that the constructChar can be built up 1 bit at a time.
                        constructChar |= (byte)(placeHolder << bitsOfConstructCharEncoded);

                        //increment counters
                        bitsOfConstructCharEncoded++;
                        numBitsThatHaveBeenEncoded++;
                        rawBytesCounter++;

                        if (bitsOfConstructCharEncoded == 8)
                        {
                            //once we have completed building our constructChar byte, we want to reset our location tracker to the first bit of the next one.
                            bitsOfConstructCharEncoded = 0;
                        }

                        if (rawBytesCounter == numBitsToBeEncoded)
                            rawBytesCounter = 0; //reset the counter because we are now starting at the first bit in the next byte.

                        //this conditional is triggered once 8 bits have been extracted from the secret bits.
                        if (numBitsThatHaveBeenEncoded == 8)
                        {
                            numBitsThatHaveBeenEncoded = 0;
                            tempConstructChar[0] = constructChar;
                            constructChar = 0;

                            //converts our byte to its ASCII form.
                            makeMessage += Encoding.ASCII.GetString(tempConstructChar);

                            //if our tempConstructChar[0] was a null character then we know we are at the end our the user's secret message.
                            if (tempConstructChar[0] == '\0')
                                loopDone = true; //set loopDone to true to kick us out of the do while.
                        }


                    }
                    currCharIndex++; //grab the next byte from our array
                }
            } while (!loopDone);

            return makeMessage;
        }

        //encodes the secret message to the bitmap
        public void setMessage(string newMessage, int chosen)
        {

            byte[] magicNumber = Encoding.ASCII.GetBytes("316"); //byte is supposed to be the closest thing to unsigned char
            byte[] byteMessage = Encoding.ASCII.GetBytes(newMessage);
            byte mask = 0x1;
            
            //an array of stripMasks that are used at different times depending on the for-loop.
            byte[] stripMasks = { 0xfe, 0xfd, 0xfb, 0xf7, 0xef, 0xdf, 0xbf, 0x7f }; 
            
            int numBitsToBeEncoded = chosen; //changes number of bits that need to be encoded depending on which button was selected.
            int numBitsThatHaveBeenEncoded = 0;

            uint currCharIndex = 0;
            int placeHolder = 0; //needed to replace the conditional ternary operation
            int currentBitInChar = 0;

            int rawBytesCounter = 0;


            //writing the magic number
            for (int i = 0; i < 3; )
            {
                for (int p = 0; p < numBitsToBeEncoded; p++)
                {

                    if (i == 3) //once we have encoded each magic number then break out of the for loops.
                        break;

                    //strips out the desired bit from the rawBytes array.
                    rawBytes[currCharIndex] &= stripMasks[rawBytesCounter];
                    if ((magicNumber[i] & (mask << currentBitInChar)) != 0) //if the bit was a non zero, then set placeholder to 1.
                    {
                        int result = (magicNumber[i] & (mask << currentBitInChar));
                        placeHolder = 1;
                    }
                    else
                    {
                        placeHolder = 0;
                    }

                    //insert the secret bit into our R/G/B field.
                    rawBytes[currCharIndex] |= (byte)(placeHolder << p);

                    //increment our counters.
                    currentBitInChar++;
                    numBitsThatHaveBeenEncoded++;
                    rawBytesCounter++;

                    
                    if (rawBytesCounter == numBitsToBeEncoded)
                    {
                        //reset the counter
                        rawBytesCounter = 0; //reset the counter because we are now starting at the first bit in the next byte.
                        currCharIndex++; //go to the next byte in our array.
                    }

                    if (numBitsThatHaveBeenEncoded == 8)
                    {
                        //once we have completed inserting a bytes worth of bits, we want to reset our location tracker to the first bit of the next field.
                        numBitsThatHaveBeenEncoded = 0;
                        currentBitInChar = 0;
                        i++; //grab the next magic number.
                    }
                }
            }

            currentBitInChar = 0;
    
            //now we begin encoding the users desired secret message.
            int secondPlaceHolder = 0;
            for (uint i = 0; i < newMessage.Length;)
            {
    
                    for (int p = rawBytesCounter; p < numBitsToBeEncoded; p++)
                    {

                        if (p == -1)
                            p = 0;

                        if (i == newMessage.Length)
                            break;

                        //strip the desired bit from the current bytes
                        rawBytes[currCharIndex] &= stripMasks[rawBytesCounter];

                        //make an array of bytes based on the newMessage string
                        if ((byteMessage[i] & (mask << currentBitInChar)) != 0)
                        {
                            secondPlaceHolder = 1; //if the stripped bit is a 1, then set secondPlaceHolder to 1.
                        }
                        else
                        {
                            secondPlaceHolder = 0; //if the stripped bit was a 0, then set secondPlaceHolder to 0.
                        }

                        //to read a bit use and, to write a bit use or
                        //insert the secret bit into our R/G/B field.
                        rawBytes[currCharIndex] |= (byte)(secondPlaceHolder << p);

                        //increment our counters
                        currentBitInChar++;
                        numBitsThatHaveBeenEncoded++;
                        rawBytesCounter++;


                        if (rawBytesCounter == numBitsToBeEncoded)
                        {
                            rawBytesCounter = 0; //reset the counter because we are now starting at the first bit in the next byte.
                            currCharIndex++;
                        }

                        if (numBitsThatHaveBeenEncoded == 8)
                        {
                            //once we have completed inserting a bytes worth of bits, we want to reset our location tracker to the first bit of the next field.
                            numBitsThatHaveBeenEncoded = 0;
                            currentBitInChar = 0;
                            i++; //grab the next character from the message.
                        }
                    }
            }

            byte stripMask = 0x1; //0001. Allows us to grab a desired bit through the use of left shifts
            int numberOfZerosAdded = 0; //keeps track of how many 0's have been added. We want to add 8.

            //this while-loop runs until 8 zeros have been added.
            while (numberOfZerosAdded < 8){
                for (int p = (rawBytesCounter); p < numBitsToBeEncoded; p++)
                {
                    if (p == -1)
                        p = 0;

                    //we want to only strip out 1 bit from the byte.
                        //(byte)~tempMask takes the compliment after it has been left shifted.
                    byte tempMask = (byte)(stripMask << rawBytesCounter);
                    tempMask = (byte)~tempMask;

                    //AND our rawByte with the tempMask
                    rawBytes[currCharIndex] &= (byte)(tempMask);

                    //increment our counter
                    numberOfZerosAdded++;

                    if (numberOfZerosAdded == 8)
                        break; //once we have added our 8 zeros, break out of the while

                    rawBytesCounter++; //increment

                    if (rawBytesCounter == numBitsToBeEncoded)
                    {
                        //grab the next byte from our rawBytes array.
                        currCharIndex++;
                        //reset the counter because we are now starting at the first bit in the next byte.
                        rawBytesCounter = 0;
                    }
                }
            }
        }

        public void writeImage(string filename)
        {
            saveBMP(filename, rawBytes); //save the bitmap to the specified file
        }

        public byte[] loadBMP(string fileName)
        {
            // Retrieve the image.
            image1 = new Bitmap(fileName, true);

            int totalNumberOfSlots = (image1.Height * image1.Width) * 3; //how many fields there are in the bitmap. each pixel has 3.
            byte[] pixelArray = new byte[totalNumberOfSlots]; //holds all the pixels of the bitmap 
            int currentIndexInArray = 0; //iterator for the pixelArray

            // Loop through the images pixels to grab each one.
            //starts in the bottom left of the bitmap and goes from left to right.
            for (int x = image1.Height - 1; x >= 0; x--)
            {
                for (int y = 0; y < image1.Width; y++)
                {
                    Color pixelColor = image1.GetPixel(y, x); //gets the color of the specific pixels
                    pixelArray[currentIndexInArray++] = pixelColor.R;
                    pixelArray[currentIndexInArray++] = pixelColor.G;
                    pixelArray[currentIndexInArray++] = pixelColor.B;
                }
            }

            return pixelArray; //give the array to be loaded into the image

        }

        void saveBMP(string fileName, byte[] pixelArray)
        {

            int currentIndexInArray = 0;//iterator to go through the array.

            for (int x = image1.Height - 1; x >= 0; x--)
            {
                for (int y = 0; y < image1.Width; y++)
                {
                    Color newColor = Color.FromArgb(pixelArray[currentIndexInArray++], pixelArray[currentIndexInArray++], pixelArray[currentIndexInArray++]); //.NET function to create the bitmap file to be stored 
                    image1.SetPixel(y, x, newColor); //sets the color of the specified pixel
                }
            }

            image1.Save(fileName); //save the image to the specified filename
        }

    }
}
